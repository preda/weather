<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import="ru.centreIT.RandomWeather.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Drop table page</title>
<%request.setCharacterEncoding("UTF-8");
if (request.getParameter("table").equals("drop")) {
	DataBaseWorker dbw = new DataBaseWorker();
	dbw.startConnection();	
	dbw.dropTable("weather");
	out.println("<p> Таблица с данными о погоде очищена.</p>");
	dbw.close();
}
%>
<a href="index.jsp">Назад</a>
</head>
<body>

</body>
</html>