<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    import ="java.sql.*"
    import ="ru.gismeteo.ws.*"
    import="ru.centreIT.RandomWeather.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Random Weather Web service!</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="header">
<img id="logo" src="img/sun2.jpg"/>
<h1>Random Weather сервис!</h1>
<p>Собирает информацию о текущей погоде в произвольных городах.</p>
<form action="weatherservlet">
<button type="submit" value="submit">Запустить парсер</button>
</form>
<form method="post" action="stop.jsp">
<input id="stop" name="parser" value="stop"/>
<button type="submit" value="submit">Остановить парсер</button>
</form>
<form id="dropTable" method="post" action="dropTable.jsp">
<input id="drop" name="table" value="drop"/>
<button type="submit" value="submit">Очистить БД</button>
</form>
</div>
<form id="selectTowns" method="post" action="index.jsp">
<button type="submit" value="submit">Показать погоду</button><br>
<% DataBaseWorker dbw = new DataBaseWorker();
dbw.startConnection(); 
for (String towns : dbw.selectTowns()) {
	out.println("<input name=\"town\" type=\"radio\" value=\"" + towns + "\">" + towns + "<br>");
	}
%>
</form>
<div id="result">
<table  style="border: 1px solid black;">
<tr>
<th>Дата/Время</th><th>Город</th><th>Температура, °С</th><th>Влажность, %</th><th>Давление, гПа</th><th>Ветер</th><th>Сервис</th>
</tr>
<% request.setCharacterEncoding("UTF-8");
ResultSet rs = dbw.selectWeather(request.getParameter("town"));
while (rs.next()) {
out.println("<tr>" +
	"<td>" + rs.getString("datetime") + "</td>" + "<td>" + rs.getString("town") + "</td>" + "<td>" + rs.getString("temperature") + "</td>" + 
	"<td>" + rs.getString("humidity") + "</td>" + "<td>" + rs.getString("pressure") + "</td>" + "<td>" + rs.getString("wind") + "</td>" +
	"<td>" + rs.getString("service") + "</td>" +
"</tr>");
}
dbw.close();%>
</table>
</div>
</body>
</html>