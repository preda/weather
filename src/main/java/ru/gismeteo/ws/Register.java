package ru.gismeteo.ws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import javax.jws.HandlerChain;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;

/**
 * This class was generated by the JAX-WS RI. JAX-WS RI 2.1.3-hudson-390-
 * Generated source version: 2.0
 * <p>
 * An example of how this class may be used:
 * 
 * <pre>
 * Register service = new Register();
 * RegisterSoap portType = service.getRegisterSoap();
 * portType.registerHHUser(...);
 * </pre>
 * 
 * </p>
 * 
 */
@WebServiceClient(name = "Register", targetNamespace = "http://ws.gismeteo.ru/", wsdlLocation = "http://ws.gismeteo.ru/Registration/Register.asmx?wsdl")
//@HandlerChain(file="handlers.xml")
public class Register extends Service {

	private final static URL REGISTER_WSDL_LOCATION;
	private final static Logger logger = Logger
			.getLogger(ru.gismeteo.ws.Register.class.getName());

	static {
		URL url = null;
		try {
			URL baseUrl;
			baseUrl = ru.gismeteo.ws.Register.class.getResource(".");
			url = new URL(baseUrl,
					"http://ws.gismeteo.ru/Registration/Register.asmx?wsdl");
		} catch (MalformedURLException e) {
			logger.warning("Failed to create URL for the wsdl Location: 'http://ws.gismeteo.ru/Registration/Register.asmx?wsdl', retrying as a local file");
			logger.warning(e.getMessage());
		}
		REGISTER_WSDL_LOCATION = url;
	}

	public Register(URL wsdlLocation, QName serviceName) {
		super(wsdlLocation, serviceName);
	}

	public Register() {
		super(REGISTER_WSDL_LOCATION, new QName("http://ws.gismeteo.ru/",
				"Register"));
	}

	/**
	 * 
	 * @return returns RegisterSoap
	 */
	@WebEndpoint(name = "RegisterSoap")
	public RegisterSoap getRegisterSoap() {
		return super.getPort(
				new QName("http://ws.gismeteo.ru/", "RegisterSoap"),
				RegisterSoap.class);
	}

	/**
	 * 
	 * @return returns RegisterSoap
	 */
	@WebEndpoint(name = "RegisterSoap12")
	public RegisterSoap getRegisterSoap12() {
		return super.getPort(new QName("http://ws.gismeteo.ru/",
				"RegisterSoap12"), RegisterSoap.class);
	}

	/**
	 * 
	 * @return returns RegisterHttpGet
	 */
	@WebEndpoint(name = "RegisterHttpGet")
	public RegisterHttpGet getRegisterHttpGet() {
		return super.getPort(new QName("http://ws.gismeteo.ru/",
				"RegisterHttpGet"), RegisterHttpGet.class);
	}

	/**
	 * 
	 * @return returns RegisterHttpPost
	 */
	@WebEndpoint(name = "RegisterHttpPost")
	public RegisterHttpPost getRegisterHttpPost() {
		return super.getPort(new QName("http://ws.gismeteo.ru/",
				"RegisterHttpPost"), RegisterHttpPost.class);
	}

}
