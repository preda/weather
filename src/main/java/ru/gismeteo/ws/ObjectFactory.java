package ru.gismeteo.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the ru.gismeteo.ws package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _RegisterResult_QNAME = new QName(
			"http://ws.gismeteo.ru/", "RegisterResult");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package: ru.gismeteo.ws
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link ServiceResult }
	 * 
	 */
	public ServiceResult createServiceResult() {
		return new ServiceResult();
	}

	/**
	 * Create an instance of {@link RegisterHHUser }
	 * 
	 */
	public RegisterHHUser createRegisterHHUser() {
		return new RegisterHHUser();
	}

	/**
	 * Create an instance of {@link RegisterMMUser }
	 * 
	 */
	public RegisterMMUser createRegisterMMUser() {
		return new RegisterMMUser();
	}

	/**
	 * Create an instance of {@link RegisterMMUserResponse }
	 * 
	 */
	public RegisterMMUserResponse createRegisterMMUserResponse() {
		return new RegisterMMUserResponse();
	}

	/**
	 * Create an instance of {@link RegisterHHUserResponse }
	 * 
	 */
	public RegisterHHUserResponse createRegisterHHUserResponse() {
		return new RegisterHHUserResponse();
	}

	/**
	 * Create an instance of {@link RegisterResult }
	 * 
	 */
	public RegisterResult createRegisterResult() {
		return new RegisterResult();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link RegisterResult }
	 * {@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://ws.gismeteo.ru/", name = "RegisterResult")
	public JAXBElement<RegisterResult> createRegisterResult(RegisterResult value) {
		return new JAXBElement<RegisterResult>(_RegisterResult_QNAME,
				RegisterResult.class, null, value);
	}

}
