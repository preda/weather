package ru.centreIT.RandomWeather;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import ru.gismeteo.ws.HHForecast;

/**
 * Класс сервлета, который собирает погоду 
 * @author Алексей
 *
 */
public class WeatherServlet extends HttpServlet implements Runnable {
	public static Thread weatherCatcher;
	private long period = 25000;
	//private static Logger logger = Logger.getLogger(WeatherServlet.class.getName());
	
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);                 
	    //weatherCatcher = new Thread(this);
	    //weatherCatcher.setPriority(Thread.MAX_PRIORITY);  
	    //weatherCatcher.start();
	}
	
	protected void doGet (HttpServletRequest request, HttpServletResponse response) {
		try {
			//if (weatherCatcher == null) {
				weatherCatcher = new Thread(this);
			//}
			weatherCatcher.start();
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			PrintWriter printWriter = response.getWriter();
			printWriter.println("<p>The random weather parser - запущен! Периодичность запросов " + period/1000 + " секунд</p>");
			printWriter.println("<a href=\"index.jsp\">Назад</a>");
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}
	
	public void setPeriod (int minutes) {
		this.period = minutes * 60 * 1000;
	}
	
	public void run () {
		GlobalWeatherClient gwc = new GlobalWeatherClient();
		ArrayList <String> threeTowns = new ArrayList <String>();
		ArrayList <String> citiesList = null;
		//while (citiesList==null) 
			try {
			citiesList = gwc.getCitiesList(); // Получаем список городов с сервиса
			
		} catch (SAXException | IOException | ParserConfigurationException e1) {
			e1.printStackTrace();
		}
		while (true) {
			try {
				DataBaseWorker dbw = new DataBaseWorker(); 
				dbw.startConnection(); // Подключаемся к БД
				threeTowns.add(citiesList.get((int) (Math.random()*citiesList.size()))); // Добавляем 3 произвольных города
				threeTowns.add(citiesList.get((int) (Math.random()*citiesList.size())));
				threeTowns.add(citiesList.get((int) (Math.random()*citiesList.size())));
				
				for (String town : threeTowns) {
					try {
						//dbw.insert(gwc.getWeatherBlock(town));   // Добавляем запись в БД (WebserviceX)
					} catch (Exception e) {
						e.printStackTrace();
					}
					for (City city : City.values()) {
						if (city.getEngCity().equals(town)) {
							town = city.getRuCity();   //Редирект с английского имени города на русское
						}
					}
					HHForecast weatherGismeteo = new GismeteoClient().getWeather(town);
					WeatherBlock weatherBlock = new WeatherBlock();
					weatherBlock.setDate(weatherGismeteo.getTime().toGregorianCalendar().getTime().getTime());
					weatherBlock.setTown(town);
					weatherBlock.setHumidity(Short.toString(weatherGismeteo.getHumidity())); 
					weatherBlock.setPressure(Double.toString(weatherGismeteo.getP()));
					weatherBlock.setTemperature(Double.toString(weatherGismeteo.getT()));
					weatherBlock.setService("Gismeteo");
					weatherBlock.setWind("-");
					dbw.insert(weatherBlock); // Добавляем запись в БД (Gismeteo)
					
				}
			} catch (IndexOutOfBoundsException | NullPointerException | ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			} finally {
				threeTowns.clear();
			}
			try {
				weatherCatcher.sleep(period);
			} catch (InterruptedException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
			threeTowns.clear();
		}
	}
	
	/*
	 * Останавливает парсер погоды
	 * 
	 */
	public void destroy() {
	    weatherCatcher.stop();
	  }
}
