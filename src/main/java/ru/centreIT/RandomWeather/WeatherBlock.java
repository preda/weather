package ru.centreIT.RandomWeather;

import java.sql.Timestamp;

/*
 * Класс описания погоды для конкретного города и времени
 */
public class WeatherBlock {
	private String date; // Дату храним в типе String, т.к. один из веб-сервисов возвращает нетиповой формат даты
	private String town;
	private String service; // Информация о том, с какого сервиса загружена погода
	private String wind;
	private String temperature;
	private String humidity;
	private String pressure;
	
	public String getWind() {
		return wind;
	}
	public void setWind(String wind) {
		this.wind = wind;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getHumidity() {
		return humidity;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public String getPressure() {
		return pressure;
	}
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = new Timestamp(date).toString();
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	
}
