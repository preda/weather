package ru.centreIT.RandomWeather;

import java.util.List;
import javax.xml.ws.WebServiceException;
import ru.gismeteo.ws.*;
/**
 * Класс для работы с сервисом погоды Gismeteo
 * @author Алексей
 *
 */
public class GismeteoClient {  
		
	public HHForecast getWeather(String town) {
		
		List<HHForecast> hHForecastList = null;
		HHForecast hHForecast = null;
		String weatherKey = getWeatherKey();  
		int weatherId = getWeatherId(town, weatherKey);
		if (weatherId!=0 && weatherKey!=null) try { 
			Weather weather = new Weather();
			WeatherSoap weatherSoap = weather.getWeatherSoap();
			HHForecastResult hHForecastResult = weatherSoap.getHHForecast(weatherKey, weatherId);
			ArrayOfHHForecast arrayOfHHForecast = hHForecastResult.getData();
			hHForecastList = arrayOfHHForecast.getHHForecast();
			hHForecast = hHForecastList.get(0);
		} catch (NullPointerException | IndexOutOfBoundsException | WebServiceException e) {
			e.printStackTrace();
		}
		return hHForecast; // current weather
	}
	
	public int getWeatherId(String town, String weatherKey) {
		int idSuccessful = 0;		
		if (weatherKey!=null) try { 
			Locations location = new Locations();
			LocationsSoap locationsSoap = location.getLocationsSoap();
			LocationInfoFullResult locationInfoFullResult = locationsSoap.findByNameFull(weatherKey, town, 10, "RU");
			ArrayOfLocationInfoFull arrayOfLocationInfoFull = locationInfoFullResult.getData();
			List<LocationInfoFull> locationInfoFull = arrayOfLocationInfoFull.getLocationInfoFull();
			idSuccessful = locationInfoFull.get(0).getId();
		}	 catch (WebServiceException e) {
			e.printStackTrace();
		}
		
		return idSuccessful;
	}
	
	public String getWeatherKey() {
		String weatherKey = null;
		try {
			Register register = new Register();
			RegisterSoap registerSoap = register.getRegisterSoap();
			RegisterResult registerResult = registerSoap.registerHHUser("Alex", "preda@pisem.net", "571258");
			weatherKey = registerResult.getKey();
		} catch (WebServiceException e) {
			e.printStackTrace();
		}
		return weatherKey;
	}
		
}
