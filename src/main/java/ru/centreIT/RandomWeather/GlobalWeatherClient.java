package ru.centreIT.RandomWeather;

import java.io.*;
import java.util.ArrayList;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import net.webservicex.*;
/*
 * Класс для работы с сервисом погоды WebserviceX
 */
public class GlobalWeatherClient {
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	DocumentBuilder db;
	/*
	 * Метод получения всех городов для России
	 */
	public ArrayList <String> getCitiesList() throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException {
		
		ArrayList <String> citiesList = null;
		try {
			GlobalWeather globalWeather = new GlobalWeather();
			GlobalWeatherSoap globalWeatherSoap = globalWeather.getGlobalWeatherSoap();
			String citiesListString =  globalWeatherSoap.getCitiesByCountry("Russia");
			dbf = DocumentBuilderFactory.newInstance();
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(new ByteArrayInputStream(citiesListString.getBytes("UTF-8")));  
			NodeList citiesListXML = doc.getElementsByTagName("City");
			citiesList = new ArrayList <String>();
			for (int i = 0; i < citiesListXML.getLength(); i++) {
				citiesList.add(citiesListXML.item(i).getTextContent());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return citiesList;			
	}
	/*
	 * Метод получения погоды для определенного города
	 */
	public WeatherBlock getWeatherBlock(String city) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException {
		
		WeatherBlock weatherBlock = null;
		try {
			GlobalWeather globalWeather = new GlobalWeather();
			GlobalWeatherSoap globalWeatherSoap = globalWeather.getGlobalWeatherSoap();
			String weatherListString =  globalWeatherSoap.getWeather(city, "Russia");
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(new ByteArrayInputStream(weatherListString.getBytes("UTF-8")));  
			NodeList weatherListXML = doc.getElementsByTagName("CurrentWeather").item(0).getChildNodes();
			weatherBlock = new WeatherBlock();
			for (int i = 0; i < weatherListXML.getLength(); i++) {
				Node node = weatherListXML.item(i);
				String nodeName = node.getNodeName();
				weatherBlock.setTown(city);
				weatherBlock.setService("WebserviceX");
				if (nodeName.equals("Time")) {
					weatherBlock.setDate(node.getTextContent());
				} else if (nodeName.equals("RelativeHumidity")) {
					weatherBlock.setHumidity(node.getTextContent());
				} else if (nodeName.equals("Pressure")) {
					weatherBlock.setPressure(node.getTextContent());
				} else if (nodeName.equals("Temperature")) {
					weatherBlock.setTemperature(node.getTextContent());
				} else if (nodeName.equals("Wind")) {
					weatherBlock.setWind(node.getTextContent());
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}
		
		return null;
	}
	
}
