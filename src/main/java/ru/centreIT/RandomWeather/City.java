package ru.centreIT.RandomWeather;

public enum City {
	
	Jakutsk("Jakutsk", "Якутск"),
	Culman("Cul'Man","Кульман"),
	Ekimchan("Ekimchan", "Экимчан"),
	Habarovsk("Habarovsk","Хабаровск"),
	Troickoe("Troickoe", "Троицкое"),
	Anadyr("Anadyr","Анадырь"),
	Buhta("Buhta Providenja","Бухта Провиденья"),
	Magadan("Magadan","Магадан"),
	Petropavlovsk("Petropavlovsk-Kamchatskij","Петропавлоск"),
	Sahalinsk("Juzhno-Sahalinsk","Сахалинск"),
	Vladivostok("Vladivostok","Владивосток"),
	Chita("Chita","Чита"),
	Irkutsk("Irkutsk","Иркутск"),
	Ordynskij("Ust'Ordynskij","Усть-Ордынский"),
	Bodajbo("Bodajbo","Бодайбо"),
	Kirensk("Kirensk","Киренск"),
	Nizhneudinsk("Nizhneudinsk","Нижнеудинск"),
	Horinsk("Horinsk","Хоринск"),
	Ulan("Ulan-Ude","Улан-Удэ"),
	Arhangelsk("Arhangel'Sk","Архангельск"),
	Kotlas("Kotlas","Котлас"),
	Peterburg("St. Peterburg","Санкт Петербург"),
	Murmansk("Murmansk","Мурманск"),
	Luki("Velikie Luki","Великие Луки"),
	Tot("Tot'Ma","Тотьма"),
	Vologda("Vologda","Волгода"),
	Barnaul("Barnaul","Барнаул"),
	Enisejsk("Enisejsk","Енисейск"),
	Novosibirsk("Novosibirsk","Новосибирск"),
	Krasnodar("Krasnodar","Краснодар"),
	Mineral("Mineral'Nye Vody","Минеральные воды"),
	Rostov("Rostov-Na-Donu","Ростов-На-Дону"),
	Adler("Adler","Адлер"),
	Elista("Elista","Элиста"),
	Volgograd("Volgograd","Волгоград"),
	HMAO("Hanty-Mansijsk","Ханты-Мансийск"),
	Surgut("Surgut","Сургут"),
	Ekaterinburg("Ekaterinburg","Екатеринбург"),
	Brjansk("Brjansk","Брянск"),
	Sheremet("Moscow / Sheremet'Ye","Шереметьево"),
	Tver("Tver","Тверь"),
	Voronez("Voronez","Воронеж"),
	Vnukovo("Moscow / Vnukovo","Внуково"),
	Ust("Ust', Kulom","Усть-Кулом"),
	Syktyvkar("Syktyvkar","Сыктывкар"),
	Penza("Penza","Пенза"),
	Samara("Samara","Самара");
	
	private String engCity;
	private String ruCity;
	
	City (String engCity, String ruCity) {
		this.setEngCity(engCity);
		this.setRuCity(ruCity);
	}

	public String getEngCity() {
		return engCity;
	}

	public void setEngCity(String engCity) {
		this.engCity = engCity;
	}

	public String getRuCity() {
		return ruCity;
	}

	public void setRuCity(String ruCity) {
		this.ruCity = ruCity;
	}

}
