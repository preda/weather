package ru.centreIT.RandomWeather;

import java.sql.*;
import java.util.ArrayList;

import ru.gismeteo.ws.HHForecast;

/**
 * Класс для работы с БД Derby: создание таблицы, запись, считывание.
 * @author Алексей
 *
 */
public class DataBaseWorker {
	Connection connection = null;
	Statement statement = null;
	
	public void startConnection() throws SQLException, ClassNotFoundException {
		Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
		connection = DriverManager.getConnection("jdbc:derby:database/weatherdb;create=true");
		statement = connection.createStatement();
		try { 
			statement.executeUpdate("CREATE TABLE weather (datetime VARCHAR(50) NOT NULL, town VARCHAR(35) NOT NULL, temperature VARCHAR(25), humidity VARCHAR(25), pressure VARCHAR(50), wind VARCHAR(50), service VARCHAR(25), PRIMARY KEY (datetime, town))");
		} catch (SQLException e) {
			//e.printStackTrace(); //table is already created
		}
	}

	/*
	 * Метод заносит данные в БД и при необходимости создает исходную таблицу
	 */
	public void insert(WeatherBlock weatherBlock) throws ClassNotFoundException, SQLException {
			
		try {
			statement.executeUpdate("INSERT INTO weather(datetime, town, temperature, humidity, pressure, wind, service) VALUES("
				+ "'" + weatherBlock.getDate() + "'," // Преобразование XMLGregorianCalnder в Timestamp для БД
				+ "'" + weatherBlock.getTown() + "',"
				+ "'" + weatherBlock.getTemperature() + "',"
				+ "'" + weatherBlock.getHumidity() + "',"
				+ "'" + weatherBlock.getPressure() + "',"
				+ "'" + weatherBlock.getWind() + "',"
				+ "'" + weatherBlock.getService() + "')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		//statement.executeUpdate("INSERT INTO test(text_e) VALUES ('HELLOE WORDL!" + (int) (Math.random()*100) +"')");
		
	}
	
	public ArrayList <String> selectTowns() throws SQLException {
		ArrayList <String> towns = new ArrayList<String>();
		try {
			ResultSet resultSet = statement.executeQuery("SELECT DISTINCT town FROM weather");
			
			while (resultSet.next()) {
			towns.add(resultSet.getString("Town"));
			//System.out.println(resultSet.getString("datetime") + " " + resultSet.getString("Town") + " " + resultSet.getString("pressure"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return towns;
	}
	public ResultSet selectWeather(String town) throws SQLException {
		ResultSet resultSet = null;
		try {
			resultSet = statement.executeQuery("SELECT * FROM weather WHERE town ='" + town + "' ORDER BY datetime");
		} catch (SQLException | NullPointerException e) {
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	/*
	 * Метод удаляет выбранную таблицу
	 */
	public void dropTable(String table) throws SQLException {
		statement.executeUpdate("DROP TABLE " + table);
	}
	
	/*
	 * Закрываем соединение с БД
	 */
	public void close() throws SQLException {
		connection.close();
	}

}
